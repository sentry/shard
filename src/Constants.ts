export enum DiscordEvents {
  shardReady = 'shardReady',
  shardResume = 'shardResume',
  shardDisconnect = 'shardDisconnect',

  messageCreate = 'messageCreate'
}
