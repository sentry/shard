require('module-alias/register');

import { Bento, FSComponentLoader } from '@ayana/bento';

import * as config from './config.json';
import * as plugins from './plugins';

import { Logger } from '@ayana/logger';
const log = Logger.get('Shard');

const bento = new Bento();

(async () => {
  bento.setVariable('config', config);

  const fsloader = new FSComponentLoader();
  await fsloader.addDirectory(__dirname, 'components');

  const p = Object.values(plugins).map(plugin => new plugin());

  await bento.addPlugins([fsloader, ...p]);
  const state = await bento.verify();

  log.info(`Bento loaded with ${state.components.length} components and ${state.plugins.length} plugins`);
})().catch(err => {
  log.error(`Error while initalizing Bento\n${err}`);
  process.exit(1);
});
