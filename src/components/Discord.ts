import { ComponentAPI, SubscribeEvent, Variable, VariableDefinitionType } from '@ayana/bento';
import * as Eris from 'eris';

import { DiscordEvents } from '@constants';

import { Logger } from '@ayana/logger';
const log = Logger.get('Discord');

export class Discord {
  public api: ComponentAPI;
  public name: string = 'Discord';

  public client: Eris.Client;

  @Variable({ type: VariableDefinitionType.OBJECT, name: 'config' })
  private config: { [key: string]: any };

  public async onLoad() {
    this.client = new Eris.Client(this.config.token, {
      autoreconnect: true,
      firstShardID: 0,
      maxShards: 1
    });

    this.api.forwardEvents(this.client, Object.values(DiscordEvents));

    await this.client.connect();
  }

  public async onUnload() {
    this.client.disconnect({ reconnect: false });
    this.client.removeAllListeners();
    this.client = null;
  }
}
